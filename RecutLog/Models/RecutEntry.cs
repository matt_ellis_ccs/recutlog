﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecutLog.Models
{
    public class RecutEntry
    {
        public int LogID { get; set; }
        public int RecutID { get; set; }
        public string WorkOrder { get; set; }
        public string Item { get; set; }
        public string Part { get; set; }
        public string RequestedBy { get; set; }
        public DateTime RequestedAt { get; set; }
        public DateTime RecutAt { get; set; }
        public int Recuts { get; set; }
        public string Comments { get; set; }
        public int AddParts { get; set; }
        public string Area { get; set; }
        public string WorkArea { get; set; }
        public string Description { get; set; }
        public string Shift { get; set; }
        public string[] PartsList { get; set; }
        public string Notes { get; set; }
    }
}