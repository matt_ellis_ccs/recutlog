﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecutLog.Models
{
    public class RecutLog
    {
        public int LogID { get; set; }
        public int RecutID { get; set; }
        public string RequestedBy { get; set; }
        public DateTime RequestedAt { get; set; }
        public DateTime RecutAt { get; set; }
        public string Comments { get; set; }
        public string Area { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
    }
}