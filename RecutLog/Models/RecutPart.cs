﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecutLog.Models
{
    public class RecutPart
    {
        public int RecutID { get; set; }
        public string WorkOrder { get; set; }
        public string Item { get; set; }
        public string Part { get; set; }
    }
}