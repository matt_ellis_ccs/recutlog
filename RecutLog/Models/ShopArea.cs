﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecutLog.Models
{
    public class ShopArea
    {
        public string Area { get; set; }
        public Boolean ListWorkAreas { get; set; }
    }
}