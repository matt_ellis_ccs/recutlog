﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecutLog.Models
{
    public class WorkArea
    {
        public string ShopArea { get; set; }
        public string WorkAreaName { get; set; }
    }
}