﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using RecutLog.Models;
using CCS_DAL;

namespace RecutLog.Controllers
{
    public class HomeController : Controller
    {
        private string connStr = @RecutLog.Properties.Settings.Default["ConnStr"].ToString();

        public ActionResult Index()
        {
            return View();
        }

        public List<string> getRecutDesc()
        {
            List<string> returnList = new List<string>();
            DAL data = new DAL(connStr);
            string selectSQL = "SELECT Description FROM RecutDescriptions ORDER BY Description";
            SqlDataReader descList = data.executeDataReader(selectSQL);

            while (descList.Read())
            {
                returnList.Add((string)descList["description"]);
            }
            return returnList;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Shop recut log.";

            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public int logCut(int logID, string comment, DAL data)
        {
            string updateSQL = @"
                    UPDATE dbo.RecutLog
                    SET RecutAt = GETDATE()
                        ,Comments = @Comments
                    WHERE logID = @logID
                ";
            SqlParameter paraID = new SqlParameter("logID", SqlDbType.Int);
            paraID.Value = logID;
            SqlParameter paraComments = new SqlParameter("Comments", SqlDbType.NVarChar);
            paraComments.Value = comment;
            SqlParameter[] logParas = new SqlParameter[] { paraID, paraComments };
            return data.executeNonQuery(updateSQL, logParas);
        }

        public int CancelCut(int logID, DAL data)
        {
            string updateSQL = @"
                    UPDATE dbo.RecutLog
                    SET Cancelled = 1
                    WHERE logID = @logID
                ";
            SqlParameter paraID = new SqlParameter("logID", SqlDbType.Int);
            paraID.Value = logID;
            SqlParameter[] cancelParas = new SqlParameter[] { paraID };
            return data.executeNonQuery(updateSQL, cancelParas);
        }

        public ActionResult RecentCut()
        {
            string workOrder = "%";
            List<RecutEntry> cutList = new List<RecutEntry>();
            DAL data = new DAL(connStr);

            if (Request.Form["btnFilter"] != null)
            {
                workOrder = Request.Form["WorkOrder"] + "%";
            }

            int limit = (int)@RecutLog.Properties.Settings.Default["RecutRows"];
            int offset = 0;
            SqlParameter paraLimit = new SqlParameter("limit", SqlDbType.Int);
            paraLimit.Value = limit;
            SqlParameter paraOffset = new SqlParameter("offset", SqlDbType.Int);
            paraOffset.Value = offset;
            SqlParameter paraWorkOrder = new SqlParameter("WorkOrder", SqlDbType.NVarChar);
            paraWorkOrder.Value = workOrder;
            SqlParameter[] listParas = new SqlParameter[] { paraLimit, paraOffset, paraWorkOrder };

            string listSQL = @"
                    SELECT
	                    log.logID
                        ,prt.recutID
	                    ,prt.WorkOrder
	                    ,prt.Item
	                    ,prt.Part
	                    ,log.RequestedBy
	                    ,log.RequestedAt
                        ,log.RecutAt
                        ,ISNULL(log.Comments, '') AS Comments
                        ,(
		                    SELECT
			                    COUNT(logID)
		                    FROM dbo.RecutLog 
		                    WHERE recutID = prt.recutID AND Cancelled = 0
		                    GROUP BY recutID
	                    ) AS Recuts
                        ,ISNULL((
		                    SELECT
			                    COUNT(Part)
		                    FROM dbo.RecutAddParts 
		                    WHERE logID = log.logID
		                    GROUP BY logID), 0
                        ) AS AddParts
                    FROM dbo.RecutParts prt
                    INNER JOIN dbo.RecutLog log ON prt.recutID = log.recutID
                    WHERE log.RecutAt IS NOT NULL AND prt.WorkOrder LIKE @WorkOrder AND log.Cancelled = 0
                    ORDER BY log.RecutAt DESC
                    OFFSET @offset ROWS
					FETCH NEXT @limit ROWS ONLY
                    ";

            SqlDataReader cutListRS = data.executeDataReader(listSQL, listParas);

            while (cutListRS.Read())
            {
                cutList.Add(new RecutEntry()
                {
                    LogID = (int)cutListRS["logID"],
                    RecutID = (int)cutListRS["recutID"],
                    WorkOrder = (string)cutListRS["WorkOrder"],
                    Item = (string)cutListRS["Item"],
                    Part = (string)cutListRS["part"],
                    RequestedBy = (string)cutListRS["RequestedBy"],
                    Recuts = (int)cutListRS["Recuts"],
                    RequestedAt = (DateTime)cutListRS["RequestedAt"],
                    RecutAt = (DateTime)cutListRS["RecutAt"],
                    Comments = (string)cutListRS["Comments"],
                    AddParts = (int)cutListRS["AddParts"]
                });
            }

            ViewData["workOrder"] = workOrder.Substring(0, workOrder.Length - 1);
            ViewData["cutList"] = cutList;

            data.close();
            return View();
        }

        public ActionResult ToBeCut()
        {
            string workOrder = "%";
            List<RecutEntry> cutList = new List<RecutEntry>();
            DAL data = new DAL(connStr);

            if (Request.HttpMethod == "POST")
            {
                if (Request.Form["btnFilter"] != null)
                {
                    workOrder = Request.Form["WorkOrder"] + "%";
                }
                if (Request.Form["btnCancel"] != null)
                {
                    int logID = 0;
                    int.TryParse(Request.Form["logID"], out logID);
                    CancelCut(logID, data);
                }
                if (Request.Form["btnCut"] != null)
                {
                    int logID = 0;
                    int.TryParse(Request.Form["logID"], out logID);
                    string comment = "";
                    if (Request.Form["Comments"] != null)
                    {
                        comment = Request.Form["Comments"];
                    }
                    logCut(logID, comment, data);
                }
            }

            int limit = (int)@RecutLog.Properties.Settings.Default["RecutRows"];
            int offset = 0;
            SqlParameter paraLimit = new SqlParameter("limit", SqlDbType.Int);
            paraLimit.Value = limit;
            SqlParameter paraOffset = new SqlParameter("offset", SqlDbType.Int);
            paraOffset.Value = offset;
            SqlParameter paraWorkOrder = new SqlParameter("WorkOrder", SqlDbType.NVarChar);
            paraWorkOrder.Value = workOrder;
            SqlParameter[] listParas = new SqlParameter[] { paraLimit, paraOffset, paraWorkOrder };

            string listSQL = @"
                    SELECT
	                    log.logID
                        ,prt.recutID
	                    ,prt.WorkOrder
	                    ,prt.Item
	                    ,prt.Part
                        ,ISNULL(prt.WorkArea, '') AS WorkArea
                        ,ISNULL(prt.Description,'') AS Description
                        ,ISNULL(prt.Area, '') AS Area
	                    ,log.RequestedBy
	                    ,log.RequestedAt
                        ,log.RecutAt
                        ,(
		                    SELECT
			                    COUNT(logID)
		                    FROM dbo.RecutLog 
		                    WHERE recutID = prt.recutID AND Cancelled = 0
		                    GROUP BY recutID
	                    ) AS Recuts
                        ,ISNULL((
		                    SELECT
			                    COUNT(Part)
		                    FROM dbo.RecutAddParts 
		                    WHERE logID = log.logID
		                    GROUP BY logID), 0
                    ) AS AddParts
                    FROM dbo.RecutParts prt
                    INNER JOIN dbo.RecutLog log ON prt.recutID = log.recutID
                    WHERE log.RecutAt IS NULL AND prt.WorkOrder LIKE @WorkOrder AND log.Cancelled = 0
                    ORDER BY log.RequestedAt
                    OFFSET @offset ROWS
					FETCH NEXT @limit ROWS ONLY
                    ";

            SqlDataReader cutListRS = data.executeDataReader(listSQL, listParas);

            while (cutListRS.Read())
            {
                string[] addParts = new string[(int)cutListRS["AddParts"]];
                if ((int)cutListRS["AddParts"] > 0)
                {
                    SqlParameter paraLogID = new SqlParameter("logID", SqlDbType.Int);
                    paraLogID.Value = (int)cutListRS["logID"];
                    SqlParameter[] partParas = new SqlParameter[] { paraLogID };
                    string partsSQL = "SELECT Part FROM RecutAddParts WHERE logID=@logID";

                    SqlDataReader partListRS = data.executeDataReader(partsSQL, partParas);

                    int recordCount = 0;
                    while (partListRS.Read())
                    {
                        addParts[recordCount] = (string)partListRS["Part"];
                        recordCount++;
                    }

                    partListRS.Close();
                }

                cutList.Add(new RecutEntry()
                {
                    LogID = (int)cutListRS["logID"]
                    ,RecutID = (int)cutListRS["recutID"]
                    ,WorkOrder = (string)cutListRS["WorkOrder"]
                    ,Item = (string)cutListRS["Item"]
                    ,Part = (string)cutListRS["part"]
                    ,RequestedBy = (string)cutListRS["RequestedBy"]
                    ,RequestedAt = (DateTime)cutListRS["RequestedAt"]
                    ,Area = (string)cutListRS["Area"]
                    ,Recuts = (int)cutListRS["recuts"]
                    ,AddParts = (int)cutListRS["AddParts"]
                    ,PartsList = addParts
                    ,WorkArea = (string)cutListRS["WorkArea"]
                });
            }
            ViewData["workOrder"] = workOrder.Substring(0, workOrder.Length - 1);
            ViewData["cutList"] = cutList;

            data.close();
            return View();
        }

        public ActionResult AddRecut()
        {
            DAL data = new DAL(connStr);

            if (Request.HttpMethod == "POST")
            {
                string errMsg = "";
                if (Request["WorkOrder"] == "")
                {
                    errMsg = "Work Order cannot be blank";
                }
                if (Request["Item"] == "")
                {
                    errMsg = "Item cannot be blank";
                }
                if (Request["Part"] == "")
                {
                    errMsg = "Part cannot be blank";
                }
                if (Request["RequestedBy"] == "")
                {
                    errMsg = "Requested By cannot be blank";
                }

                if (errMsg != "")
                {
                    ViewData["ErrMsg"] = errMsg;
                    return View();
                }

                SqlParameter paraWorkOrderSearch = new SqlParameter("@WorkOrder", SqlDbType.NVarChar);
                paraWorkOrderSearch.Value = Request["WorKOrder"];
                SqlParameter paraItemSearch = new SqlParameter("@Item", SqlDbType.NVarChar);
                paraItemSearch.Value = Request["Item"];
                SqlParameter paraPartSearch = new SqlParameter("@Part", SqlDbType.NVarChar);
                paraPartSearch.Value = Request["Part"];
                SqlParameter[] searchParas = new SqlParameter[] { paraWorkOrderSearch, paraItemSearch, paraPartSearch };
                
                int insertedID = 0;
                string searchSQL = @"
                            SELECT recutID 
                            FROM dbo.RecutParts 
                            WHERE WorkOrder = @WorkOrder AND Item = @Item AND Part = @Part;
                        ";

                string workArea = "";
                if (Request["Area"] == "Machining")
                {
                    workArea = Request["WorkArea"];
                }

                SqlDataReader searchRS = data.executeDataReader(searchSQL, searchParas);

                SqlParameter paraWorkOrder = new SqlParameter("@WorkOrder", SqlDbType.NVarChar);
                paraWorkOrder.Value = Request["WorKOrder"];
                SqlParameter paraItem = new SqlParameter("@Item", SqlDbType.NVarChar);
                paraItem.Value = Request["Item"];
                SqlParameter paraPart = new SqlParameter("@Part", SqlDbType.NVarChar);
                paraPart.Value = Request["Part"];
                SqlParameter paraArea = new SqlParameter("@Area", SqlDbType.NVarChar);
                paraArea.Value = Request["Area"];
                SqlParameter paraWorkArea = new SqlParameter("@WorkArea", SqlDbType.NVarChar);
                paraWorkArea.Value = workArea;
                SqlParameter paraDescription = new SqlParameter("@Description", SqlDbType.NVarChar);
                paraDescription.Value = Request["Description"];
                SqlParameter paraNotes = new SqlParameter("Notes", SqlDbType.NVarChar);
                paraNotes.Value = Request["Notes"];
                SqlParameter[] insertParas = new SqlParameter[] { paraWorkOrder, paraItem, paraPart, paraArea, paraDescription, paraWorkArea, paraNotes };

                if (searchRS.HasRows)
                {
                    searchRS.Read();
                    insertedID = searchRS.GetInt32(0);
                }
                else
                {
                    string insertStr = @"
                            INSERT INTO dbo.RecutParts
                                    (WorkOrder
                                    ,Item
                                    ,Part
                                    ,Area
                                    ,Description
                                    ,WorkArea
                                    ,Notes)
                                OUTPUT inserted.recutID
                                VALUES
                                    (@WorkOrder
                                    ,@Item
                                    ,@Part
                                    ,@Area
                                    ,@Description
                                    ,@WorkArea
                                    ,@Notes);
                            ";
                    object inserted = data.executeScalar(insertStr, insertParas);
                    insertedID = int.Parse(inserted.ToString());
                }
                searchRS.Close();

                SqlParameter paraRecutID = new SqlParameter("@recutID", SqlDbType.Int);
                paraRecutID.Value = insertedID;
                SqlParameter paraRequestedBy = new SqlParameter("@RequestedBy", SqlDbType.NVarChar);
                paraRequestedBy.Value = Request["RequestedBy"];

                SqlParameter[] logParas = new SqlParameter[] { paraRecutID, paraRequestedBy };

                string insertLog = @"
                            INSERT INTO dbo.RecutLog
                                (recutID
                                ,RequestedBy)
                            OUTPUT inserted.logID
                            VALUES(@recutID
                                ,@RequestedBy);
                        ";

                object logID = data.executeScalar(insertLog, logParas);
                int insertedLogID = int.Parse(logID.ToString());

                string insertAdd = @"
                            INSERT INTO dbo.RecutAddParts
                                (logID
                                ,Part)
                            VALUES (@logID
                                ,@Part);
                        ";

                int counter = 1;
                while (counter < 11)
                {
                    string curLabel = "AddPart" + counter.ToString();
                    if (Request.Form[curLabel] != "")
                    {
                        SqlParameter paralogID = new SqlParameter("@logID", SqlDbType.Int);
                        paralogID.Value = insertedLogID;
                        SqlParameter paraAddPart = new SqlParameter("@Part", SqlDbType.NVarChar);
                        paraAddPart.Value = Request.Form[curLabel];

                        SqlParameter[] AddPartParas = new SqlParameter[] { paralogID, paraAddPart };

                        int added = data.executeNonQuery(insertAdd, AddPartParas);
                    }
                    counter++;
                }

                

                ViewData["WorkOrder"] = Request["WorkOrder"];
            }
            List<string> recutDesc = getRecutDesc();

            //pass areas for lists
            List<ShopArea> ShopAreaList = new List<ShopArea>();
            string shopAreasSQL = "SELECT Area, ListWorkAreas FROM dbo.ShopAreas ORDER BY Area;";
            SqlDataReader shopAreas = data.executeDataReader(shopAreasSQL);
            while (shopAreas.Read())
            {
                ShopAreaList.Add(new ShopArea()
                {
                    Area = (string)shopAreas["Area"]
                    ,ListWorkAreas = (Boolean)shopAreas["ListWorkAreas"]
                });
            }

            ViewData["shopAreas"] = ShopAreaList;

            List<WorkArea> WorkAreaList = new List<WorkArea>();
            string workAreasSQL = "SELECT ShopArea, WorkArea FROM dbo.WorkAreas ORDER BY ShopArea;";
            SqlDataReader workAreas = data.executeDataReader(workAreasSQL);
            while (workAreas.Read())
            {
                WorkAreaList.Add(new WorkArea()
                {
                    ShopArea = (string)workAreas["ShopArea"]
                    ,WorkAreaName = (string)workAreas["WorkArea"]
                });
            }

            ViewData["recutDesc"] = recutDesc;
            ViewData["workAreas"] = WorkAreaList;

            data.close();
            return View();
        }

        public ActionResult WorkOrderCheck()
        {
            DAL data = new DAL(connStr);

            if (Request.HttpMethod == "POST")
            {
                string errMsg = "";
                if (Request["WorkOrder"] == "")
                {
                    errMsg = "Work Order cannot be blank";
                }
                if (Request["RequestedBy"] == "")
                {
                    errMsg = "Rework Runner By cannot be blank";
                }

                if (errMsg != "")
                {
                    ViewData["ErrMsg"] = errMsg;
                    return View();
                }

                for (int i = 1; i <= 25; i++)
                {
                    if (Request["Item" + i .ToString()] != "")
                    {
                        string curItem = Request["Item" + i.ToString()];
                        string curArea = Request["Area" + i.ToString()];
                        string curDescription = Request["Description" + i.ToString()];
                        string curPart = Request["PArt" + i.ToString()];
                        string curCabinetType = Request["CabinetType" + i.ToString()];
                        int curDoors = 1;
                        if (Request["Doors" + i.ToString()] == null)
                        {
                            curDoors = 0;
                        }
                        int curDrawers = 1;
                        if (Request["Drawers" + i.ToString()] == null)
                        {
                            curDrawers = 0;
                        }

                        string curWorkArea = "";
                        if (curArea == "Machining")
                        {
                            curWorkArea = Request["WorkArea" + i.ToString()];
                        }

                        string curNotes = "";
                        if (Request["Notes" + i.ToString()] == null)
                        {
                            curWorkArea = Request["Notes" + i.ToString()];
                        }

                        SqlParameter paraWorkOrder = new SqlParameter("@WorkOrder", SqlDbType.NVarChar);
                        paraWorkOrder.Value = Request["WorKOrder"];
                        SqlParameter paraCutList = new SqlParameter("@CutList", SqlDbType.NVarChar);
                        paraCutList.Value = Request["CutList"];
                        SqlParameter paraRequestedBy = new SqlParameter("@RequestedBy", SqlDbType.NVarChar);
                        paraRequestedBy.Value = Request["RequestedBy"];
                        SqlParameter paraItem = new SqlParameter("@Item", SqlDbType.NVarChar);
                        paraItem.Value = curItem;
                        SqlParameter paraPart = new SqlParameter("@Part", SqlDbType.NVarChar);
                        paraPart.Value = curPart;
                        SqlParameter paraArea = new SqlParameter("@Area", SqlDbType.NVarChar);
                        paraArea.Value = curArea;
                        SqlParameter paraDescription = new SqlParameter("@Description", SqlDbType.NVarChar);
                        paraDescription.Value = curDescription;
                        SqlParameter paraCabinetType = new SqlParameter("@CabinetType", SqlDbType.NVarChar);
                        paraCabinetType.Value = curCabinetType;
                        SqlParameter paraDoors = new SqlParameter("@Doors", SqlDbType.Bit);
                        paraDoors.Value = curDoors;
                        SqlParameter paraDrawers = new SqlParameter("@Drawers", SqlDbType.Bit);
                        paraDrawers.Value = curDrawers;
                        SqlParameter paraWorkArea = new SqlParameter("@WorkArea", SqlDbType.NVarChar);
                        paraWorkArea.Value = curWorkArea;
                        SqlParameter paraNotes = new SqlParameter("@Notes", SqlDbType.NVarChar);
                        paraNotes.Value = curNotes;

                        SqlParameter[] insertParas = new SqlParameter[] { paraWorkOrder, paraCutList, paraItem, paraPart,
                            paraArea, paraDescription, paraCabinetType, paraDoors, paraDrawers, paraWorkArea, paraNotes };

                        string insertStr = @"
                                            INSERT INTO dbo.RecutParts
                                                (WorkOrder
                                                ,CutList
                                                ,Item
                                                ,Part
                                                ,Area
                                                ,Description
                                                ,CabinetType
                                                ,Doors
                                                ,Drawers
                                                ,WorkArea
                                                ,Notes)
                                            OUTPUT inserted.recutID
                                            VALUES
                                                (@WorkOrder
                                                ,@CutList
                                                ,@Item
                                                ,@Part
                                                ,@Area
                                                ,@Description
                                                ,@CabinetType
                                                ,@Doors
                                                ,@Drawers
                                                ,@WorkArea
                                                ,@Notes);
                                        ";

                        object inserted = data.executeScalar(insertStr, insertParas);
                        int insertedID = int.Parse(inserted.ToString());

                        SqlParameter paraRecutID = new SqlParameter("@recutID", SqlDbType.Int);
                        paraRecutID.Value = insertedID;

                        SqlParameter[] logParas = new SqlParameter[] { paraRecutID, paraRequestedBy };

                        string insertLog = @"
                            INSERT INTO dbo.RecutLog
                                (recutID
                                ,RequestedBy)
                            OUTPUT inserted.logID
                            VALUES(@recutID
                                ,@RequestedBy);
                        ";

                        object logID = data.executeScalar(insertLog, logParas);
                    }
                }

                ViewData["WorkOrder"] = Request["WorkOrder"];
            }

            //pass areas for lists
            List<ShopArea> ShopAreaList = new List<ShopArea>();
            string shopAreasSQL = "SELECT Area, ListWorkAreas FROM dbo.ShopAreas ORDER BY Area;";
            SqlDataReader shopAreas = data.executeDataReader(shopAreasSQL);
            while (shopAreas.Read())
            {
                ShopAreaList.Add(new ShopArea()
                {
                    Area = (string)shopAreas["Area"]
                    ,
                    ListWorkAreas = (Boolean)shopAreas["ListWorkAreas"]
                });
            }

            ViewData["shopAreas"] = ShopAreaList;

            List<WorkArea> WorkAreaList = new List<WorkArea>();
            string workAreasSQL = "SELECT ShopArea, WorkArea FROM dbo.WorkAreas ORDER BY ShopArea;";
            SqlDataReader workAreas = data.executeDataReader(workAreasSQL);
            while (workAreas.Read())
            {
                WorkAreaList.Add(new WorkArea()
                {
                    ShopArea = (string)workAreas["ShopArea"]
                    ,
                    WorkAreaName = (string)workAreas["WorkArea"]
                });
            }
            List<string> recutDesc = getRecutDesc();

            ViewData["recutDesc"] = recutDesc;
            ViewData["workAreas"] = WorkAreaList;

            data.close();
            return View();
        }
    }
}